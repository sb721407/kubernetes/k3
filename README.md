## SkillBox DevOps. Инфраструктурная платформа на основе Kubernetes. Часть 3. Основные объекты Kubernetes

SkillBox DevOps. Инфраструктурная платформа на основе Kubernetes. Часть 3. Основные объекты Kubernetes

--------------------------------------------------------

Сценарий работы с репозиторием:
```
cd existing_repo
git remote add origin git@gitlab.com:sb721407/kubernetes/k3.git
git branch -M main
git push -uf origin main
```

### Задание 1

Исправить номер порта в сервисе:

https://gitlab.com/sb721407/kubernetes/k3/-/tree/main/task1


### Задание 2 

https://gitlab.com/sb721407/kubernetes/k3/-/tree/main/task2
